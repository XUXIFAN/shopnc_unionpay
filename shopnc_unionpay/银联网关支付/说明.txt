该插件是shopnc（好商城V3/V4）版本中的中国银联网关支付业务。

online_pay文件夹放置于shop/api/payment文件夹中，该文件夹中的unionpay.php文件中需要修改各自公司的商户代码（见文件中代码提示）。请将文件夹名改成unionpay，此处是为了区别另一个文件夹临时改名。

unionpay文件夹请放置于data/api文件夹中。其中SDK文件夹中的sdkconfig.php需要修改（见注释说明）。certs文件夹中放置银联证书。

相关文件的开头的include_once中，请修改后面引用文件的路径。

最后，请在数据库的payment数据表中添加一条数据。	id自增，paymeny_code为“unionpay”， payment_name为“网关支付”。这样即可在后台的“设置”--“支付方式”中开启和关闭银联网关支付。并能在页面看到相应支付选项。

注：请事先做好相关备份，并在装上文件后进行调试。